# Stefans Weblog

Bug 🐜 and issue 🟥 tracker
If you encountered a bug or have a feature request feel free to create an [issue](https://codeberg.org/sst/website-issues/issues).

***

<a href="https://www.paypal.com/donate?hosted_button_id=BNV5XKAAXK6TJ"><img src="https://img.shields.io/badge/Kaffee%20via-PayPal-blue.svg" /></a> 😊 <a href="https://liberapay.com/strobelstefan.de/donate"><img src="https://img.shields.io/badge/kaffee%20via-liberapay-yellow"></a> 😎 <img src="https://img.shields.io/badge/Kaffee%20via%20-Bitcoin-orange.svg" /> bc1qfuz93hw2fhdvfuxf6mlxlk8zdadvnktppkzqzj 😄

***

🐘 [Mastodon](https://mastodon.social/@strobelstefan) &bull; 🐘 [![Mastodon RSS](https://mastodon.social/@strobelstefan.rss)](https://mastodon.social/@strobelstefan.rss) &bull; [![RSS](https://img.shields.io/badge/Follow-RSS-orange)](https://strobelstefan.de/feed/)

***
